<?php
/**
 * Selective Case plugin for Craft CMS 3.x
 *
 * Change selected words to different case
 *
 * @link      https://matrixcreate.com
 * @copyright Copyright (c) 2022 Jamie Adams
 */

namespace matrixcreate\selectivecase\twigextensions;

use matrixcreate\selectivecase\SelectiveCase;

use Craft;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * @author    Jamie Adams
 * @package   SelectiveUppercase
 * @since     0.0.1
 */
class SelectiveCaseTwigExtension extends AbstractExtension
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'SelectiveCase';
    }

    /**
     * @inheritdoc
     */
    public function getFilters()
    {
        return [
            new TwigFilter('someFilter', [$this, 'someInternalFunction']),
            new TwigFilter('selectiveCase', [$this, 'SelectiveCase']),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('someFunction', [$this, 'someInternalFunction']),
        ];
    }

    /**
     * @param null $text
     *
     * @return string
     */
    public function someInternalFunction($text = null)
    {
        $result = $text . " in the way";

        return $result;
    }

    /**
     * @param null $text
     *
     * @return string
     */
    public function SelectiveCase($text = null)
    {
        $settings = \matrixcreate\selectivecase\SelectiveCase::getInstance()->settings->text;
        
        foreach ($settings as $key => $value) {
            $text = str_ireplace($value, strtoupper($value), $text);            
        }

        return $text;
    }
}
