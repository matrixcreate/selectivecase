<?php

namespace matrixcreate\selectivecase\models;

use craft\base\Model;

class Settings extends Model
{
   public $text = '';

   public function rules()
   {
      return [
         [['text'], 'required'],
         // ...
      ];
   }
}