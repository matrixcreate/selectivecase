<?php
/**
 * Selective Case plugin for Craft CMS 3.x
 *
 * Change selected words to different case
 *
 * @link      https://matrixcreate.com
 * @copyright Copyright (c) 2022 Jamie Adams
 */

/**
 * @author    Jamie Adams
 * @package   SelectiveCase
 * @since     0.0.1
 */
return [
    'Selective Case plugin loaded' => 'Selective Case plugin loaded',
];
