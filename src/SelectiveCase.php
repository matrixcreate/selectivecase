<?php
/**
 * Selective Uppercase plugin for Craft CMS 3.x
 *
 * Change selected words to uppercase
 *
 * @link      https://matrixcreate.com
 * @copyright Copyright (c) 2022 Jamie Adams
 */

namespace matrixcreate\selectivecase;

use matrixcreate\selectivecase\twigextensions\SelectiveCaseTwigExtension;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\events\RegisterCpNavItemsEvent;
use craft\web\twig\variables\Cp;
use yii\base\Event;

/**
 * Class SelectiveCase
 *
 * @author    Jamie Adams
 * @package   SelectiveCase
 * @since     0.0.1
 *
 */
class SelectiveCase extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var SelectiveCase
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '0.0.1';

    /**
     * @var bool
     */
    public $hasCpSettings = false;

    /**
     * @var bool
     */
    public $hasCpSection = false;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        Craft::$app->view->registerTwigExtension(new SelectiveCaseTwigExtension());

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                }
            }
        );

        /* Event::on(
            Cp::class,
            Cp::EVENT_REGISTER_CP_NAV_ITEMS,
            function(RegisterCpNavItemsEvent $event) {
                $event->navItems[] = [
                    'url' => 'section-url',
                    'label' => 'Section Label',
                    'icon' => '@mynamespace/path/to/icon.svg',
                ];
            }
        ); */

        Craft::info(
            Craft::t(
                'selective-case',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================
    protected function createSettingsModel()
    {
        return new \matrixcreate\selectivecase\models\Settings();
    }
}
