# Selective Uppercase plugin for Craft CMS 3.x

Change selected words to uppercase (for now)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require matrixcreate/selective-uppercase

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for Selective Case.

## Selective Case Overview

Supply a list of strings in a config file and use the `|selectiveCase` filter to convert those given strings to uppercase within the supplied text.

## Configuring Selective Case

Create a `selective-case.php` file within `\config` directory for Craft.

Within the config file supply an array of strings using the key of `text` to convert to uppercase where the filter is used.

```
<?php

return [
   'text' => [
      'abc',
      'def'
   ]
];
```

## Using Selective Case

Append the `|selectiveCase` filter to a Twig output e.g. `{{ entry.title|selectiveCase }}`

## Selective Case Roadmap

Some things to do, and ideas for potential features:

* Release it
* Add Control Panel settings page for the plugin
* Add a Control Panel menu for the plugin
* Allow for case option e.g. `|selectiveCase('lower')` to lower the text
* Provide an override within Twig e.g. `|selectiveCase('title', ['hello','world'])`


Brought to you by [Matrix Create](https://matrixcreate.com)
